import os

from difflib import SequenceMatcher
from functools import reduce
from math import gcd
from ndspy.rom import NintendoDSRom
from skytemple_files.common.types.file_types import FileType
from skytemple_files.dungeon_data.mappa_bin.protocol import (
    MappaFloorProtocol,
    MappaItemListProtocol,
    GUARANTEED,
)
from skytemple_files.hardcoded.dungeons import HardcodedDungeons
from skytemple_files.common.util import get_ppmdu_config_for_rom, get_binary_from_rom


class DungeonInfo:
    def __init__(
        self,
        name: str,
        direction: int,
        id: int,
        mappaId: int,
        floors: int,
        startAfter: int,
    ) -> None:
        self.name = name
        self.direction = direction
        self.id = id
        self.mappaId = mappaId
        self.floors = floors
        self.startAfter = startAfter


class SearchResultInfo:
    def __init__(
        self,
        floorNum: int,
        locationType: str,
        itemName: str,
        itemId: int,
        dungeonInfo: DungeonInfo,
        probability: float,
    ) -> None:
        self.floorNum = floorNum
        self.locationType = locationType
        self.itemName = itemName
        self.itemId = itemId
        self.dungeonInfo = dungeonInfo
        self.probability = probability


# Begin Init
if os.path.exists("./rom.nds"):
    filePath = "./rom.nds"
else:
    filePath = input("Enter the path for the rom: ")
ndsRom = NintendoDSRom.fromFile(filePath.strip("\"'"))
mappaBin = ndsRom.getFileByName("BALANCE/mappa_s.bin")
mappa = FileType.MAPPA_BIN.deserialize(mappaBin)
ppmdu = get_ppmdu_config_for_rom(ndsRom)
arm9Bin = get_binary_from_rom(ndsRom, ppmdu.bin_sections.arm9)
dungeons = HardcodedDungeons.get_dungeon_list(arm9Bin, ppmdu)
restrictions = HardcodedDungeons.get_dungeon_restrictions(arm9Bin, ppmdu)
langBin = ndsRom.getFileByName(
    f"MESSAGE/{ppmdu.string_index_data.languages[0].filename}"
)
lang = FileType.STR.deserialize(langBin, string_encoding=ppmdu.string_encoding)
dungeonStringBlock = ppmdu.string_index_data.string_blocks["Dungeon Names (Main)"]
itemStringBlock = ppmdu.string_index_data.string_blocks["Item Names"]
itemCategories = ppmdu.dungeon_data.item_categories

aggr_dungeon_info: list[DungeonInfo] = []
for i in range(len(dungeons)):
    dungeonName = lang.strings[dungeonStringBlock.begin + i]
    aggr_dungeon_info.append(
        DungeonInfo(
            dungeonName,
            restrictions[i].direction.value,
            i,
            dungeons[i].mappa_index,
            dungeons[i].number_floors,
            dungeons[i].start_after,
        )
    )


# End Init


# Begin Methods
@staticmethod
def calculate_relative_weights(list_of_weights: list[int]) -> list[int]:
    """Given a list of absolute spawn weights, return the relative values."""
    weights = []
    if len(list_of_weights) < 1:
        return []
    for i in range(0, len(list_of_weights)):
        weight = list_of_weights[i]
        if weight != 0:
            last_nonzero = i - 1
            while last_nonzero >= 0 and list_of_weights[last_nonzero] == 0:
                last_nonzero -= 1
            if last_nonzero != -1:
                weight -= list_of_weights[last_nonzero]
        weights.append(weight)
    weights_nonzero = [w for w in weights if w != 0]
    weights_gcd = 1
    if len(weights_nonzero) > 0:
        weights_gcd = reduce(gcd, weights_nonzero)
    return [int(w / weights_gcd) for w in weights]


@staticmethod
def calculate_item_probability(
    itemId: int, itemCategory: int, itemList: MappaItemListProtocol
) -> float:
    if itemId not in itemList.items:
        return 0
    if itemList.items[itemId] == GUARANTEED:
        return 100

    floorRelativeWeights = calculate_relative_weights(
        list(itemList.categories.values())
    )
    categoryWeightSum = sum(floorRelativeWeights)
    categoryWeightSum = categoryWeightSum if categoryWeightSum > 0 else 1
    itemCategoryWeight = 0
    for i, (category, _) in enumerate(itemList.categories.items()):
        if category == itemCategory:
            itemCategoryWeight = floorRelativeWeights[i]
    itemsInCategory = [
        (item, prob)
        for (item, prob) in itemList.items.items()
        if itemCategories[itemCategory].is_item_in_cat(item)
    ]
    weightsInCategory = [prob for _, prob in itemsInCategory if prob != GUARANTEED]
    itemCategoryRelativeWeights = calculate_relative_weights(weightsInCategory)
    itemCategoryWeightSum = sum(itemCategoryRelativeWeights)
    itemCategoryWeightSum = itemCategoryWeightSum if itemCategoryWeightSum > 0 else 1
    itemProb = 0
    for i, item in enumerate(
        [item for item, prob in itemsInCategory if prob != GUARANTEED]
    ):
        if item == itemId:
            itemProb = itemCategoryRelativeWeights[i]

    categoryProb = itemCategoryWeight / float(categoryWeightSum)
    itemProbInCategory = itemProb / float(itemCategoryWeightSum)
    return categoryProb * itemProbInCategory


# End Methods

searchItemStr = input("Enter the item id or item name you want to search for: ")
if searchItemStr.isnumeric():
    searchItemId = int(searchItemStr)
else:
    matchData: dict[int, float] = dict()
    for i, itemName in enumerate(
        lang.strings[itemStringBlock.begin : itemStringBlock.end]
    ):
        similarity = SequenceMatcher(None, searchItemStr, itemName).ratio()
        matchData[i] = similarity
    sortedMatchData = dict(
        sorted(matchData.items(), key=lambda item: item[1], reverse=True)
    )
    print("Found the following search results:")
    sortedItemIds = list(sortedMatchData)[:3]
    for i in range(3):
        print(f"{i + 1}: {lang.strings[itemStringBlock.begin + sortedItemIds[i]]}")
    selectedItemInput = input("Enter the number of the item to search for: ")
    if not selectedItemInput.isnumeric():
        print("Invalid entry, assuming exit requested.")
        exit()
    searchItemId = sortedItemIds[int(selectedItemInput) - 1]
categoryIdx = 0

for catIdx, category in itemCategories.items():
    if category.is_item_in_cat(searchItemId):
        categoryIdx = catIdx

dungeonIndex = 0
associatedDungeon = aggr_dungeon_info[dungeonIndex]
for i in range(len(mappa.floor_lists)):
    foundItemsInfo: list[SearchResultInfo] = []
    for floorIdx in range(len(mappa.floor_lists[i])):
        floor: MappaFloorProtocol = mappa.floor_lists[i][floorIdx]
        if (i == 0 and floorIdx == 0) or (
            dungeonIndex + 1 < len(aggr_dungeon_info)
            and floorIdx == aggr_dungeon_info[dungeonIndex + 1].startAfter
        ):
            dungeonIndex += 1 if i > 0 else 0
            associatedDungeon = aggr_dungeon_info[dungeonIndex]
            if len(foundItemsInfo) > 0:
                for foundItemInfo in foundItemsInfo:
                    floorPrefix = (
                        "BF" if foundItemInfo.dungeonInfo.direction == 0 else "F"
                    )
                    print(
                        f"    {floorPrefix}{foundItemInfo.floorNum}: '{foundItemInfo.itemName}' {foundItemInfo.locationType} {foundItemInfo.probability * 100:.2f}%"
                    )
                foundItemsInfo.clear()
            print(f"{associatedDungeon.name}:")
        buriedItems: MappaItemListProtocol = floor.buried_items

        # Floor Items
        floorItems: MappaItemListProtocol = floor.floor_items
        floorItemProb = calculate_item_probability(
            searchItemId, categoryIdx, floorItems
        )
        if floorItemProb > 0:
            searchResult = SearchResultInfo(
                floorIdx + 1,
                "Floor",
                lang.strings[itemStringBlock.begin + searchItemId],
                searchItemId,
                associatedDungeon,
                floorItemProb,
            )
            foundItemsInfo.append(searchResult)

        monsterHouseItems: MappaItemListProtocol = floor.monster_house_items
        shopItems: MappaItemListProtocol = floor.shop_items

    if len(foundItemsInfo) > 0:
        for foundItemInfo in foundItemsInfo:
            floorPrefix = "BF" if foundItemInfo.dungeonInfo.direction == 0 else "F"
            print(
                f"    {floorPrefix}{foundItemInfo.floorNum}: '{foundItemInfo.itemName}' {foundItemInfo.locationType} {foundItemInfo.probability * 100:.2f}%"
            )
